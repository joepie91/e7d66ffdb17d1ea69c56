function namedLog(name) {
	return function gutilLog(log) {
		gc = gutil.colors;

		items = ["[- " + gc.magenta(name) + " -]"];

		for(i in arguments) {
			items.push(arguments[i]);
		}

		gutil.log.apply(null, items);
	}
}