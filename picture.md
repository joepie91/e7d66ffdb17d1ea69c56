The `Buffer` output comes from `namedLog`, and makes it easier to determine whether a change was picked and compiled up correctly.

![](https://i.imgur.com/JRWklBy.png)