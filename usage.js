// [...]

var spy = require("through2-spy");

// [...]

	return gulp.src(subTask.source, {base: subTask.base})
		.pipe(plumber())
		.pipe(cache(taskName))
		.pipe(processor.on('error', gutil.log)) // `processor` refers to eg. gulp-jade or whatever
		.pipe(spy.obj(namedLog(taskName))) // this does the actual logging, using namedLog defined above.
		.pipe(remember(taskName))
		.pipe(gulp.dest(subTask.destination));
		
// [...]